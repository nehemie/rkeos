# Rpackage rkeos

Status: [![pipeline status](https://gitlab.com/nehemie/rkeos/badges/dev/pipeline.svg)](https://gitlab.com/nehemie/rkeos/commits/dev)
<!-- [![coverage report](https://gitlab.com/nehemie/rkeos/badges/data/coverage.svg)](https://gitlab.com/nehemie/rkeos/commits/data) -->

`rkeos` is a R package to facilitate transparency and reproducibility of
statistical analysis and mapping of archaeological surveys data-sets.

The package strives to

  - Give access to functions crafted for the visualisations of data
  during/directly after the fieldwork. Useful to write preliminary
  reports/funding applications.
  
  - Offer more advanced functions (in future release).

  - Provide a standardised data-set as a template for other surveys
    and demonstrate the possibilities of the package's functions
through examples.  The data-set was largely influenced by the
[Panormos Project Survey](http://www.panormos.de/pp/data/) data-set.

  - Create a platform for meta-analysis of surveys, allowing quick
    access to already published data. If you have a data-set that you
would like to get included in the package, let me know and open an
[issue](https://gitlab.com/nehemie/rkeos/issues).

## Installing `rkeos`

You need an installation of R (see [install R](https://r-project.org))
Install the R package using the following commands on the R console:

```
require(devtools)
devtools::install_git("https://gitlab.com/nehemie/rkeos.git")
library(rkeos)
```

## Getting started

TBD

<!--
```
data(raw_data)
res = AnomalyDetectionTs(raw_data, max_anoms=0.02, direction='both', plot=TRUE)
res$plot
```

![Fig 1](https://github.com/twitter/AnomalyDetection/blob/master/figs/Fig1.png)
-->


## Licence

GPL 3: see file LICENCE in root directory
