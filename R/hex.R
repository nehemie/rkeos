#' @title rk_pcounts_to_grid
#'
#' @name rk_pcounts_to_grid
#' @rdname rk_pcounts_to_grid
#' @docType methods
#' @description Make a grid for rk_pcount
#'
#' @param object  Rkeos_intensive class object
#'
#' @param cell_area numeric, The area
#'
#' @param type character. Square or Hexagonal
#'
#' @param minimal logical. Reduce footprint by returning only polygons
#' with values
#'
#' @return A spatial polygon
#'
#' @importFrom rgeos gUnaryUnion gConvexHull gIntersection
#' @importFrom sp SpatialPolygonsDataFrame spsample geometry
#' @importFrom sp HexPoints2SpatialPolygons coordinates over
#' @importFrom raster extent rasterize values values<- projection
setGeneric("rk_pcounts_to_grid",
           function(object, cell_area, type, minimal = TRUE)
             standardGeneric("rk_pcounts_to_grid"))

#' @rdname rk_pcounts_to_grid
#' @aliases rk_pcounts_to_grid,Rkeos_intensive-method
#' @examples
#' rk_pcounts_to_grid(rk_data, cell_area = 625, "square")
#' rk_pcounts_to_grid(rk_data, 2000, "hex")
#'
#' @export
setMethod("rk_pcounts_to_grid",
          signature(object = "Rkeos_intensive"),
          definition = function(object, cell_area, type, minimal) {
            #object <- pps

            # else if (type == "hex") {
            #  cell_width <- sqrt(2 * cell_area / sqrt(3))

            tracts <- as(rk_get_tracts(object), "Spatial")
            counts <- as(rk_get_counts(object), "Spatial")
            contours <- rgeos::gUnaryUnion(tracts)
            box <- rgeos::gConvexHull(tracts)

            if (type == "square") {
              cell_width <- sqrt(cell_area)
              r <- raster::raster(raster::extent(tracts) + 0.1,
                                  resolution = cell_width)
              raster::projection(r) <- raster::projection(tracts)
              r <- raster::rasterize(sp::coordinates(counts), r,
                                     fun = "count", background = 0)
              names(r) <- "pottery_count"
              if (minimal == TRUE) {
              raster::values(r)[values(r) == 0] <- NA
              polyg <- as(r, "SpatialPolygonsDataFrame")
              } else {
              polyg <- as(r, "SpatialPolygonsDataFrame")
              polyg <- rgeos::gIntersection(polyg, contours,
                                            byid = TRUE,
                                            drop_lower_td = TRUE)
              n_counts <- data.frame(pottery_count =
                                      values(r)[!is.na(values(r))])
              polyg <- sp::SpatialPolygonsDataFrame(polyg, n_counts,
                                                    match.ID = FALSE)
              }
              return(polyg)
            }

            if (type == "hex") {
              cell_width <- sqrt(2 * cell_area / sqrt(3))
              boxhex <- sp::spsample(box, type = "hexagonal",
                                     cellsize = cell_width,
                                     offset = c(0.5, 0.5))
              boxhex <- sp::HexPoints2SpatialPolygons(boxhex)
              if (minimal == TRUE) {
              hex <- boxhex
              pottery_count <- sapply(sp::over(hex,
                                              sp::geometry(counts),
                                              returnList = TRUE),
                                     length)
              pottery_count[pottery_count == "0"] <- NA
              pottery_count <- na.omit(pottery_count)
              # To hex sp
              hex_pottery <-
                sp::SpatialPolygonsDataFrame(hex[names(pottery_count)],
                                             as.data.frame(pottery_count),
                                             match.ID = TRUE)
              } else {
              hex <- rgeos::gIntersection(boxhex,  contours, byid = TRUE)
              pottery_count <- sapply(sp::over(hex, sp::geometry(counts),
                                         returnList = TRUE), length)
              hex_pottery <- sp::SpatialPolygonsDataFrame(hex[names(n_counts)],
                                            as.data.frame(pottery_count),
                                            match.ID = TRUE)
              }
              return(hex_pottery)
            }
            }
          )


#' @title rk_pcounts_to_finds
#'
#' @name rk_pcounts_to_finds
#' @rdname rk_pcounts_to_finds
#' @docType methods
#' @description Make a grid for rk_pcount
#'
#' @param object  Rkeos_intensive class object
#'
#' @param spshape sp SpatialPoly
#'
#' @return Rkeos_intensive class object
#'
setGeneric("rk_pcounts_to_finds",
           function(object, spshape)
             standardGeneric("rk_pcounts_to_finds"))

#' @rdname rk_pcounts_to_finds
#' @aliases rk_pcounts_to_finds,Rkeos_intensive-method
#' @examples
#' spshape <- rk_pcounts_to_grid(rk_data, cell_area = 625, "square")
#' rk_data <- rk_pcounts_to_finds(rk_data, spshape)
#
#' @importFrom sp over
#' @export
setMethod("rk_pcounts_to_finds",
          signature(object = "Rkeos_intensive"),
          definition = function(object, spshape) {
             finds <- rk_get_finds(object)
             finds <- as(finds, "Spatial")
             cell_finds <- sp::over(finds, spshape, returnList = FALSE)
             cell_finds <- cell_finds[[1]]
             #cell_finds <- cut(cell_finds, 5, labels = c(11,13,15,17,19))
             #finds$weight <- as.numeric(as.character(cell_finds))
             finds$weight <- cell_finds
             finds <- st_as_sf(finds)
             object@finds <- finds
             return(object)
})
